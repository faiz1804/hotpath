#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <stdio.h>

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
	/****Variables Used****/
	char flag = '0';
	int frameCnt = 5;
	int inc = 5;
	int xCor, yCor, xROISize=20, yROISize=20;
	Mat frame, first, temp;
	Vec3b Color;
	Vec3b Color1;
	CascadeClassifier human_cascade;
	
	/****Load the Trained Cascade Classifier XML file****/
	human_cascade.load(argv[1]);

	/****Open up the Video Ports or Provide path to Video file****/
	VideoCapture cap(argv[2]);
	if(!cap.isOpened()) {
		cout << "Cannot open the video file" << endl;
   		return -1;
    }

	/****Windows to display the Original and processed videos****/
	namedWindow("Frame", CV_WINDOW_NORMAL);
	namedWindow("test", CV_WINDOW_NORMAL);

	/****Run until Video length or interrupted by user by pressing ESC key****/
	while(1)
	{
		
		/*bool bSuccess = cap.read(frame);
		if(!bSuccess) {
			cout << "Cannot read the frame from the video file" << endl;
			break;
	    }*/

		/****Grab frames****/
		for (int i=0; i<frameCnt ; i++) {
			cap.grab();
		}

		/****Read frames - Every 6th frame is read for faster processing****/
		bool bSuccess = cap.retrieve(frame);
		if(!bSuccess) {
			cout << "Cannot read the frame from the video file" << endl;
			break;
		}


		/****Declare a Image matrix with zeros to provide as a base for generating the heatmap****/
		if(flag == '0') {
			temp = Mat::zeros(frame.size().height, frame.size().width, CV_8UC3);
			first = frame.clone();
			flag = '1'; 
		}

		vector<Rect> human;

		/****Detect and Mark the humans. Update the Zero Image Matrix according to the movement****/
		human_cascade.detectMultiScale(frame, human, 1.1, 2, 0 | 1, Size(30,50), Size(80, 300));
		if (human.size() > 0) {
			for (int i=0; i<human.size(); i++) {
				rectangle(frame, human[i].tl(), human[i].br(), Scalar(0,0,255), 2, 8, 0);
				//xCor = ((human[i].tl().x + human[i].br().x) * 0.5);
				xCor = human[i].tl().x + 10;
				yCor = human[i].br().y - 20;
				for (int j=0; j<xROISize; j++) {
					for (int k=0; k<yROISize; k++) {
						circle(frame, Point(xCor+j, yCor+k), 2, Scalar(0,255,0),CV_FILLED,LINE_8,0);
						Color = temp.at<Vec3b>(Point(xCor+j, yCor+k));
						if (Color[0] < 255) {
							Color[0] = Color[0] + inc;
						} else {
							continue;
						}
						if (Color[1] < 255) {
							Color[1] = Color[1] + inc;
						} else {
							continue;
						}
						if (Color[2] < 255) {
						Color[2] = Color[2] + inc;
						} else {
							continue;
						}
						temp.at<Vec3b>(Point(xCor+j, yCor+k)) = Color;
					}
				}
			}
		}

		imshow("Frame", frame);
		imshow("test", temp);

		/****Apply Color Map to convert to heat map and overlay on the first template image****/
		if(waitKey(30)==27) {
			applyColorMap(temp, temp, COLORMAP_JET);
			addWeighted(temp, 0.5, first, 0.5, 0, first);
			imwrite("image.png", first);
			cout << "End" << endl;
			break;
		}
	}

	cap.release();
	return 0;
}
